# pipeline-templates

Collection of `.gitlab-ci.yml` templates for inclusion in other projects which collectively utilize common workflows / jobs

## Using a template from this project

You can use a template from this project by adding a [CI yaml 'include'](https://docs.gitlab.com/ee/ci/yaml/#include).

For most purposes, a simple include is probably sufficient:

```yaml
include:
  - project: 'gitlab-com/gl-infra/pipeline-templates'
    file: '/templatefile.yml'
```

More advanced usage can be found in the CI yaml 'include' documentation linked earlier.

## Development of templates

### Testing job execution locally

**Note:** this gitlab-runner exec approach only works for simple jobs that
don't depend on a specific runner environment or environment variables.

If you would like to test templates locally, without waiting for merges and
pipelines, then you can temporarily (and locally) replace the contents of a
project's CI yaml with your template's contents. Then you can use [a
local install of GitLab Runner](https://docs.gitlab.com/runner/install/), and
run `gitlab-runner exec docker <job_name>` within the project directory. This
should pull in the necessary Docker images, and then execute your job and verify
basic functionality.

Make note of the [limiations of 'gitlab-runner exec'](https://docs.gitlab.com/runner/commands/#limitations-of-gitlab-runner-exec) if you use this aproach.
